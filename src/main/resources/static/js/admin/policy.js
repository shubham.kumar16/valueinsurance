
$(document).ready(
	function() {
		$("#policy").submit(function(event) {
			event.preventDefault();
			ajaxPost();
		});
		function ajaxPost() {
			var company = $("#company").val();
			var data = new FormData($("#policy")[0]);
			data.append('company', company);

			$.ajax({
				type: 'POST',
				enctype: 'multipart/form-data',
				url: "/admin/addPolicy",
				data: data,
				processData: false,
				contentType: false,
				cache: false,
				success: function(response) {
					alert("stored  successfully");
					console.log(response);
					$(document).ajaxStop(function() {
						window.location.reload();
					});

				},
				error: function(response) {
					console.log(response);
					alert("Oops! something went wrong.");
				}
			});
		}
	});


$(document).ready(function() {
	$("#getPolicy").submit(function(event) {
		event.preventDefault();
		fetchPolicy();
	});
});


$(document).ready(function() {
	fetchPolicy();
});


function fetchPolicy() {
	var keyword = $("#keyword").val();
	var tableData = "";
	$.ajax({
		type: "GET",
		url: "/admin/getPolicies",
		data: { keyword: keyword },
		success: function(response) {
			console.log(response);

			response.forEach(function(item, index) {
				tableData += '<tr>' +
					'<td id = "slNo' + item.id + '">' + (index + 1) + '</td>' +
					'<td id = "name' + item.id + '">' + item.name + '</td>' +
					'<td id = "companyName' + item.id + '">' + item.company.name + '</td>' +
					'<td id = "year' + item.id + '">' + item.year + '</td>' +
                    '<td id = "price' + item.id + '">' + item.price + '</td>' +
                    '<td id = "preview' + item.id + '">' + '<img src="/policyImages/' + item.imageName + '"+"height="100px" width="100px"">' + '</td>' +
					'<td id = "update' + item.id + '">' + '<a href="/admin/policy/update/' + item.id + '"+" class = "btn btn-warning btn-md "">' + 'Update' + '</td>' +
					'<td>' +
					'<button type = "button" id = "deleteButton' + item.id + '" class = "btn btn-danger btn-md deleteButton" onclick = "deletePolicy(' + item.id + ')">Delete</button>' +
					'</td>' +
					'</tr>';
			});
			$("#policy-table>tbody").html(tableData);
		},
		error: function(response) {
			console.log(response);
		}
	});
}


function deletePolicy(id) {
	if (confirm("are you want to delete ?")) {
		console.log(id);
		$.ajax({
			type: "DELETE",
			url: "/admin/deletePolicy",
			data: { id: id },
			success: function(response) {
				console.log(response);
				fetchPolicy();
			},
			error: function(response) {
				console.log(response);
				alert("oops something went to wrong");
			}
		});
	} else {
		console.log();
	}
}

