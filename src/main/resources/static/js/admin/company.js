// for post company in to database

$(document).ready(
	function() {
		$("#postcompany").submit(function(event) {
			event.preventDefault();
			ajaxPost();
		});
		function ajaxPost() {
			var formData = {
				name: $("#name").val()

			}
			$.ajax({
				type: "POST",
				contentType: "application/json",
				url: "/admin/addCompany",
				data: JSON.stringify(formData),
				dataType: 'json',
				success: function(response) {
					$(document).ajaxStop(function() {
						console.log(response);
						window.location.reload();
					});
					fetchCompany();
				},

				error: function(response) {
					console.log(response);
				}
			});
		}
	})


$(document).ready(function() {
	$("#getCompany").submit(function(event) {
		event.preventDefault();
		fetchCompany();
	});
});


$(document).ready(function() {
	fetchCompany();
})


// get companies from database

function fetchCompany() {

	var keyword = $("#keyword").val();
	var tableData = "";
	$.ajax({
		type: "GET",
		url: "/admin/getCompanies",
		data: { keyword: keyword },
		success: function(response) {
			console.log(response);

			response.forEach(function(item,index) {
				tableData += '<tr>' +
					'<td id = "slNo' + item.id + '">' + (index+1) + '</td>' +
					'<td id = "name' + item.id + '">' + item.name + '</td>' +
					'<td>' +
					'<button type = "button" id = "editButton' + item.id + '" class = "btn btn-warning btn-md editButton" onclick = "editCompany(' + item.id + ')">Update</button>&nbsp;' +
					'<button type = "button" id = "updateButton' + item.id + '" style = "display:none;" class = "btn btn-success btn-md updateButton" onclick = "updateCompany(' + item.id + ')">Save</button>' +
					'</td>' +
					'<td>' +
					'<button type = "button" id = "deleteButton' + item.id + '" class = "btn btn-danger btn-md deleteButton" onclick = "deleteCompany(' + item.id + ')">Delete</button>' +
					
					'</td>' +
					'</tr>';
			});
			$("#company-table>tbody").html(tableData);
		},
		error: function(response) {
			console.log(response);
		}
	});

}

// delete company from database

function deleteCompany(companyId) {


	if (confirm("are you want to delete ?")) {
		$.ajax({
			type: "DELETE",
			url: "/admin/deleteCompany",
			data: { id: companyId },
			success: function(response) {
				console.log(response);
				fetchCompany();
			},
			error: function(response) {
				console.log(response);
				alert("cannot delete directly company while inside this company policy will present");
			}

		});
	} else {
		console.log();
		fetchCompany();
	}

}


// edit company


function editCompany(companyId) {
	if (confirm("are you want to update ?")) {
		document.getElementById("editButton" + companyId).style.display = "none";
		document.getElementById("updateButton" + companyId).style.display = "block";

		var name = document.getElementById("name" + companyId);  //  td nameid 
		var name_data = name.innerHTML;
		console.log(name_data)

		name.innerHTML = "<input type='text' id='name_text" + companyId + "' value='" + name_data + "'>";
	} else {
		console.log();
		fetchCompany();
	}


}

// after update save company in to database

function updateCompany(companyId) {

	var name = $('#name_text' + companyId).val();
	
	$.ajax({
		type: "PUT",
		url: "/admin/updateCompany",
		data: { id: companyId, name: name },
		success: function(response) {
			console.log(response);
			fetchCompany();
		},
		error: function(response) {
			console.log(response);
		}

	});
}

// for validation

function printError(elemId, hintMsg) {
	document.getElementById(elemId).innerHTML = hintMsg;
}

function submitForm() {
	document.getElementById("postcompany").submit();
}

// Defining a function to validate form 
function validateForm() {
	// Retrieving the values of form elements 
	var name = document.postcompany.name.value;

	// Defining error variables with a default value
	var nameErr = true;

	// Validate name
	if (name == "") {
		printError("nameErr", "Please enter company name");
	} else {
		var regex = /^[a-zA-Z\s]+$/;
		if (regex.test(name) === false) {
			printError("nameErr", "Please enter a valid name");
		} else {
			printError("nameErr", "");
			nameErr = false;
		}
	}

	// Prevent the form from being submitted if there are any errors
	if (nameErr == true) {
		return false;
	} else {
		// Creating a string from input data for preview
		var dataPreview = "You've entered the following details: \n" +
			"Full Name: " + name + "\n";

		// Display input data in a dialog box before submitting the form
		alert(dataPreview);
		submitForm();
	}
};




