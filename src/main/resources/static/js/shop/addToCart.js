$(document).ready(function() {
	$("#buttonAddToCart").on("click", function(e) {
		addToCart();
	});
});

function addToCart() {
	var year = $("#year" + policyId).val();
	url = contextPath + "cart/add/" + policyId + "/" + year;

	$.ajax({
		type: "POST",
		url: url,
	}).done(function(response) {
		console.log(response);
		alert(year + " item successfully added to your cart");
	}).fail(function(response) {
		console.log(response);
		alert("you are not logged in.");
	});
}