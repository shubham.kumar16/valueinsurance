$(document).ready(function() {
	$(".minusButton").on("click", function(evt) {
		evt.preventDefault();
		decreaseYear($(this));
	});

	$(".plusButton").on("click", function(evt) {
		evt.preventDefault();
		increaseYear($(this));
	});

	$(".link-remove").on("click", function(evt) {
		evt.preventDefault();
		removeFromCart($(this));
	});

	updateTotal();
});


function removeFromCart(link) {
	url = link.attr("href");
	$.ajax({
		type: "POST",
		url: url,
		success: function(response) {
			$(document).ajaxStop(function() {
				window.location.reload();
			});
		},
	}).done(function(newSubtotal) {
		updateSubtotal(newSubtotal, policyId);
		updateTotal();

	}).fail(function(response) {
		alert("item not added, something wrong");
	});

}

function decreaseYear(link) {
	policyId = link.attr("pid");
	qtyInput = $("#year" + policyId);
	newQty = parseInt(qtyInput.val()) - 1;

	if (newQty > 0) {
		qtyInput.val(newQty);
		updateYear(policyId, newQty);
	}
}

function increaseYear(link) {
	policyId = link.attr("pid");
	qtyInput = $("#year" + policyId);
	newQty = parseInt(qtyInput.val()) + 1;

	if (newQty < 6) {
		qtyInput.val(newQty);
		updateYear(policyId, newQty);
	}
}

function updateYear(policyId, year) {
	url = contextPath + "cart/update/" + policyId + "/" + year;

	$.ajax({
		type: "POST",
		url: url,
		success: function(response) {

		},
	}).done(function(newSubtotal) {
		updateSubtotal(newSubtotal, policyId);
		updateTotal();
	}).fail(function(response) {
		alert("item not added, something wrong");
	});

}

function updateSubtotal(newSubtotal, policyId) {
	$("#subtotal" + policyId).text(newSubtotal);
}

function updateTotal() {
	var total = 0;

	$(".policySubtotal").each(function(index, element) {
		console.log(total);
		total = total + parseFloat(element.innerHTML);
		console.log(total);
	});
	$("#totalAmount").html(total);
	total = $("#totalAmount").text();
	$.ajax({
		type: "POST",
		url: "/cart/total/" + total,
	}).done(function(response) {

	}).fail(function(response) {

	});

}