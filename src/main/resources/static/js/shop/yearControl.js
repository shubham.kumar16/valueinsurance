$(document).ready(function(){
	$(".minusButton").on("click",function(evt){
		evt.preventDefault();
		policyId=$(this).attr("pid");
		qtyInput = $("#year"+policyId);
		newQty = parseInt(qtyInput.val())-1;
		
		if(newQty >0 ) qtyInput.val(newQty);
		
	});
	
	$(".plusButton").on("click",function(evt){
		evt.preventDefault();
		policyId=$(this).attr("pid");
		qtyInput = $("#year"+policyId);
		newQty = parseInt(qtyInput.val())+1;
		
		if(newQty <6 ) qtyInput.val(newQty);
		
	});
});
