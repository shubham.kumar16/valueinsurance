package com.vinove.ValueInsurance.service;

import java.util.List;

import javax.transaction.Transactional;

import com.vinove.ValueInsurance.model.CartItem;
import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.model.User;
import com.vinove.ValueInsurance.repository.CartItemRepository;
import com.vinove.ValueInsurance.repository.PolicyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
@Transactional
public class ShoppingCartServices {

	@Autowired
	private CartItemRepository cartItemRepository;

	@Autowired
	private PolicyRepository policyRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(ShoppingCartServices.class);

	public List<CartItem> listCartItems(int i){
		return cartItemRepository.findByUser_Id(i);
	}

	public int addPolicy(int policyId, int year, User user) {
		int addedYear = year;
		Policy policy = policyRepository.findById(policyId).get();

		CartItem cartItem = cartItemRepository.findByUserAndPolicy(user, policy);
		if(cartItem != null) {
			addedYear = cartItem.getYear()+year;
			cartItem.setYear(addedYear);
		}else {
			cartItem = new CartItem();
			cartItem.setYear(year);
			cartItem.setUser(user);
			cartItem.setPolicy(policy);
		}
		cartItemRepository.save(cartItem);
		return addedYear;
	}

	public float updateYear(int policyId,int year,User user) {
		cartItemRepository.updateYear(year, policyId, user.getId());

		Policy policy = policyRepository.findById(policyId).get();
		float subtotal = policy.getPrice() * year;
		return subtotal;
	}

	public void removePolicy(int policyId,User user) {
		try {		
			cartItemRepository.deleteByUserAndPolicy(user.getId(), policyId);
		}catch (Exception e) {
			logger.info("content not found");
		}
	}

	public void deleteAllPolicyByUser_Id(User user) {
		try {		
			cartItemRepository.deleteAllPolicyByUser(user.getId());
		}catch (Exception e) {
			logger.info("content not found");
		}
	}
	
	public List<CartItem> findByUser_Id(int i){
		return cartItemRepository.findByUser_Id(i);
	}
}
