package com.vinove.ValueInsurance.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.vinove.ValueInsurance.model.Company;
import com.vinove.ValueInsurance.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class CompanyService {

	@Autowired
	private CompanyRepository companyRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyService.class);

	public void addCompany(Company company) {
		try {
			companyRepository.save(company);
		} catch (Exception e) {
			logger.warn("invalid definition exception, Please fill all details carefully");
		}
	}

	public Company getCompanyById(int id) {
		return companyRepository.getByCompanyId(id);
	}

	public Company getCompanyByName(String name) {
		return companyRepository.getByName(name);
	}

	public List<Company> getAllCompany() {
		return companyRepository.findAll();
	}

	public void removeCompanyById(int id) {
		try {
			companyRepository.deleteById(id);
		} catch (Exception e) {
			logger.info("id not found");
		}
	}
	
	public Optional<Company> findCompanyByName(String name){
		return companyRepository.findCompanyByName(name);
	}

	public List<Company> getCompanyByKeyword(String keyword) {
		return companyRepository.getCompanyByKeyword(keyword);
	}

}
