package com.vinove.ValueInsurance.service;

import java.util.Optional;

import javax.transaction.Transactional;

import com.vinove.ValueInsurance.model.Address;
import com.vinove.ValueInsurance.repository.AddressRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(AddressService.class);

	public void postAddress(Address address) {
		try {
			addressRepository.save(address);
		} catch (Exception e) {			
			logger.warn("invalid definition exception, Please fill all details carefully");
		}
	}
	
	public Optional<Address> findAddressByUserId(int userId){
		return addressRepository.findAddressByUserId(userId);
	}
}
