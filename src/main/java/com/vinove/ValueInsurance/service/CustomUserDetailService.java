package com.vinove.ValueInsurance.service;

import java.util.Optional;

import com.vinove.ValueInsurance.model.CustomUserDetail;
import com.vinove.ValueInsurance.model.User;
import com.vinove.ValueInsurance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
;

@Service
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findUserByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("user not found"));
		return user.map(CustomUserDetail::new).get();
	}

}
