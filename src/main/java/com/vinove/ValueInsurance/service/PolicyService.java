package com.vinove.ValueInsurance.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.repository.PolicyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class PolicyService {

	@Autowired
	private PolicyRepository policyRepository;

	private static final Logger logger = LoggerFactory.getLogger(PolicyService.class);
	
	public void addPolicy(Policy policy) {
		try {
			policyRepository.save(policy);
		} catch (Exception e) {
			logger.warn("invalid, validation exception, Please fill all details carefully");
		}

	}
	
	public Optional<Policy> getPolicyById(int id){
		return policyRepository.findById(id);
	}
	
	public List<Policy> getAllPolicy(){
		return policyRepository.findAll();
	}
	
	public List<Policy> getAllPoliciesByCompanyId(int id){
		return policyRepository.findAllByCompany_Id(id);
	}
	
	public List<Policy> listAll(String keyword) {
		return policyRepository.listAll(keyword);
	}
	
	public List<Policy> findPolicyByCompany_IdAndKeyword(int id,String keyword){
		return policyRepository.findPolicyByCompany_IdAndKeyword(id,keyword);
	}

	public List<Policy> findByCompany_IdAndPriceBetween(int id,int min,int max){
		return policyRepository.findByCompany_IdAndPriceBetween(id, min, max);
	}

	public void removePolicyById(int id) {
		try {
			policyRepository.deleteById(id);
		} catch (Exception e) {
			logger.warn("Data Integrity Violation Exception");
		}
	}
	
	public Policy getById(int id) {
		return policyRepository.getById(id);
	}
	
	public Optional<Policy> findById(int id) {
		return policyRepository.findById(id);
	}
	
	public void save(Policy policy) {
		policyRepository.save(policy);
	}

	public List<Policy> getPolicyByKeyword(String keyword) {
		return policyRepository.getPolicyByKeyword(keyword);
		
	}
	
}
