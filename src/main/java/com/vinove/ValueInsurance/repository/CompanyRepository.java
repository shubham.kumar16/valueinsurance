package com.vinove.ValueInsurance.repository;

import java.util.List;
import java.util.Optional;

import com.vinove.ValueInsurance.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

	public Company getByName(String name);

	public Optional<Company> findCompanyByName(String name);

	@Query("SELECT c from Company c WHERE id=?1")
	public Company getByCompanyId(Integer id);

	@Query("SELECT c from Company c WHERE c.name LIKE %?1%")
	public List<Company> getCompanyByKeyword(String keyword);

}
