package com.vinove.ValueInsurance.repository;

import java.util.List;

import com.vinove.ValueInsurance.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;



public interface OrderRepository extends JpaRepository<Order, Integer>{

	public List<Order> findAllByUser_Id(int id);
	
	public List<Order> findAllByCustomerOrderId(String CustomerOrderId);

}
