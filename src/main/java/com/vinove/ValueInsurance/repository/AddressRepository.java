package com.vinove.ValueInsurance.repository;

import java.util.Optional;

import com.vinove.ValueInsurance.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address, Integer>{

	public Optional<Address> findAddressByUserId(int userId);

}
