package com.vinove.ValueInsurance.repository;

import java.util.Optional;

import com.vinove.ValueInsurance.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	public Optional<User> findUserByEmail(String email);

	public User getUserById(int id);

}
