 package com.vinove.ValueInsurance.repository;

import java.util.List;
import java.util.Optional;

import com.vinove.ValueInsurance.model.CartItem;
import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Integer>{

	public List<CartItem> findByUser(Object object);

	public List<CartItem> findByUser(Optional<User> user);

	public List<CartItem> findByUser_Id(int i);

	public List<CartItem> findById(int i);

	public CartItem findByUserAndPolicy(User user, Policy policy);

	@Query("UPDATE CartItem c SET c.year = ?1 WHERE c.policy.id = ?2 "
			+"AND c.user.id = ?3")
	@Modifying
	public void updateYear(int year,int policyId,int user_Id);

	@Query("DELETE FROM CartItem c WHERE c.user.id = ?1 AND c.policy.id = ?2")
	@Modifying
	public void deleteByUserAndPolicy(int user_Id,int policyId);

	@Query("DELETE FROM CartItem c WHERE c.user.id = ?1")
	@Modifying
	public void deleteAllPolicyByUser(int user_Id);


}
