package com.vinove.ValueInsurance.repository;

import java.util.List;

import com.vinove.ValueInsurance.model.Policy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface PolicyRepository extends JpaRepository<Policy, Integer>{

	public List<Policy> findAllByCompany_Id(int id);

	@Query("SELECT p from Policy p WHERE p.name LIKE %?1%"
			+" OR p.description LIKE %?1%"
			+" OR p.imageName LIKE %?1%")
	public List<Policy> listAll(String keyword);
	
	@Query("SELECT p from Policy p WHERE p.company.id =?1 AND (p.name LIKE %?2%"
			+" OR p.description LIKE %?2%"
			+" OR p.imageName LIKE %?2%)")
	public List<Policy> findPolicyByCompany_IdAndKeyword(int id,String keyword);


	public List<Policy> findByCompany_IdAndPriceBetween(int id,int min,int max);

	@Query("SELECT p from Policy p WHERE p.name LIKE %?1%"
			+" OR p.description LIKE %?1%"+" OR p.company.name LIKE %?1%"
			+" OR p.imageName LIKE %?1%")
	public List<Policy> getPolicyByKeyword(String keyword);

}
