package com.vinove.ValueInsurance.repository;

import com.vinove.ValueInsurance.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
