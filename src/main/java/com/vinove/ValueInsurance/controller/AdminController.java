package com.vinove.ValueInsurance.controller;

import com.vinove.ValueInsurance.dto.PolicyDTO;
import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.service.CompanyService;
import com.vinove.ValueInsurance.service.PolicyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class AdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	public static String uploadDir = System.getProperty("user.dir")+"/src/main/resources/static/policyImages";

	@Autowired
	private CompanyService companyService;

	@Autowired
	private PolicyService policyService;

	// admin home page
	@RequestMapping(path = "/admin")
	public String adminHome() {
		return "admin/adminHome";
	}

	
	// companies view page
	@RequestMapping("/admin/companies/add")
	public String getCompanyAdd() {
		return "admin/companiesAdd";
	}


	
	// policies view page
	@GetMapping("/admin/policies")
	public String policies() {
		return "admin/policies";
	}

	// add policy page model
	@GetMapping("/admin/policies/add")
	public String policyAddGet(Model model) {
		model.addAttribute("policyDTO", new PolicyDTO());
		model.addAttribute("companies",companyService.getAllCompany());
		return "admin/policiesAdd";
	}

	// update policy page model
	@GetMapping("/admin/policy/update/{id}")
	public String updatePolicy(@PathVariable int id,Model model) {
		try {
			Policy policy = policyService.getPolicyById(id).get();
			PolicyDTO policyDTO = new PolicyDTO();
			policyDTO.setId(policy.getId());
			policyDTO.setName(policy.getName());
			policyDTO.setCompanyId(policy.getCompany().getId());
			policyDTO.setPrice(policy.getPrice());
			policyDTO.setYear(policy.getYear());
			policyDTO.setClaimable(policy.getClaimable());
			policyDTO.setDescription(policy.getDescription());
			policyDTO.setImageName(policy.getImageName());

			model.addAttribute("companies",companyService.getAllCompany());
			model.addAttribute("policyDTO",policyDTO);
			return "admin/policiesAdd";
		}catch (Exception e) {
			logger.info("content not found");
			return "admin/policiesAdd";
		}
	}
}
