package com.vinove.ValueInsurance.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.vinove.ValueInsurance.model.Company;
import com.vinove.ValueInsurance.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AdminCompanyRestController {

	private static final Logger logger = LoggerFactory.getLogger(AdminCompanyRestController.class);

	@Autowired
	private CompanyService companyService;

	
	// Add company in to database
	@RequestMapping(method = RequestMethod.POST, value = "/admin/addCompany")
	public ResponseEntity<Company> addCompany(@Valid @RequestBody Company company) {
		try {
			if (companyService.findCompanyByName(company.getName()).isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			} else {
				company.setName(company.getName());
				companyService.addCompany(company);
				logger.info("company added successfully");
				return ResponseEntity.status(HttpStatus.CREATED).body(company);
			}
		} catch (Exception e) {
			logger.warn("please enter Name");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}

	// Get all companies from database
	@RequestMapping(method = RequestMethod.GET, value = "/admin/getCompanies")
	public ResponseEntity<List<Company>> getAllCategories(@RequestParam String keyword) {
		if (keyword == null) {
			List<Company> list = companyService.getAllCompany();
			if (list.size() <= 0) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
			}
			return ResponseEntity.of(Optional.of(list));
		}else {
			List<Company> list = companyService.getCompanyByKeyword(keyword);
			if (list.size() <= 0) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
			}
			return ResponseEntity.of(Optional.of(list));
		}
	}

	// Delete company in to database
	@RequestMapping(method = RequestMethod.DELETE, value = "/admin/deleteCompany")
	public ResponseEntity<Void> removeCompany(@RequestParam("id") int id) {
		try {
			companyService.removeCompanyById(id);
			logger.info("company deleted successfully");
			return ResponseEntity.status(HttpStatus.ACCEPTED).build();
		} catch (Exception e) {
			logger.warn("cannot delete directly company while inside this company policy will present");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	// Update company in to database
	@RequestMapping(method = RequestMethod.PUT, value = "/admin/updateCompany")
	public ResponseEntity<Company> updateCompany(@Valid @RequestParam("id") int id,
			@RequestParam("name") String name) {
		Company company = new Company();
		try {
			company.setId(id);
			company.setName(name);
			companyService.addCompany(company);
			logger.info("company added successfully");
			return ResponseEntity.status(HttpStatus.CREATED).body(company);
		} catch (Exception e) {
			logger.warn("company name will not empty");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}
}
