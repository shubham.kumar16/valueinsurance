package com.vinove.ValueInsurance.controller;

import java.util.List;

import com.vinove.ValueInsurance.model.CartItem;
import com.vinove.ValueInsurance.model.User;
import com.vinove.ValueInsurance.service.ShoppingCartServices;
import com.vinove.ValueInsurance.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ShoppingCartController {

	@Autowired
	private ShoppingCartServices cartService;

	@Autowired
	private UserService userService;

	// view page of cart
	@GetMapping("/cart")
	public String showShoppingCart(Model model,@AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
        authentication = securityContext.getAuthentication();
		User user = userService.getCurrentLoggedInUser(authentication);
		User dbUser = userService.findUserByEmail(user.getEmail()).get();
		List<CartItem> cartItems = cartService.listCartItems(dbUser.getId());
		model.addAttribute("cartItems",cartItems);
		model.addAttribute("pagerTitle","Shopping Cart");
		return "shoppingCart";
	}
}
