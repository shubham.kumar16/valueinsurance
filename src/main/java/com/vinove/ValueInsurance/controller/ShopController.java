package com.vinove.ValueInsurance.controller;

import java.util.List;

import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.service.CompanyService;
import com.vinove.ValueInsurance.service.PolicyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ShopController {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private PolicyService policyService;

	private static final Logger logger = LoggerFactory.getLogger(ShopController.class);

	// home page
	@GetMapping({ "/", "/home" })
	public String home(Model model) {
		return "index";
	}
	
	// shop home page with filter 
	@GetMapping("/shop")
	public String shop(Model model, @Param("keyword") String keyword) {
		if (keyword == null) {
			model.addAttribute("Companies", companyService.getAllCompany());
			model.addAttribute("policies", policyService.getAllPolicy());
			return "shop";
		} else {
			List<Policy> listPolicies = policyService.listAll(keyword);
			model.addAttribute("keyword", keyword);
			model.addAttribute("Companies", companyService.getAllCompany());
			model.addAttribute("listPolicies", listPolicies);
			return "shop";
		}
	}
	
	// shop home page company according to id, price and filter
	@GetMapping("/shop/company/{id}")
	public String shopByCompany(Model model, @PathVariable int id, @Param("keyword") String keyword,
			@Param("min") String min, @Param("max") String max) {

		try {
			if (keyword == null) {
				if ((min == "" && max == "") || (min == null && max == null)) {
					model.addAttribute("Companies", companyService.getAllCompany());
					model.addAttribute("name", companyService.getCompanyById(id).getName());
					model.addAttribute("policies", policyService.getAllPoliciesByCompanyId(id));
					return "shop";
				}else{
					List<Policy> listPolicies = policyService.findByCompany_IdAndPriceBetween(id,
							Integer.parseInt(min), Integer.parseInt(max));
					model.addAttribute("Companies", companyService.getAllCompany());
					model.addAttribute("name", companyService.getCompanyById(id).getName());
					model.addAttribute("listPolicies", listPolicies);
					return "shop";
				}
			} else {
				List<Policy> listPolicies = policyService.findPolicyByCompany_IdAndKeyword(id,keyword);
				model.addAttribute("keyword", keyword);
				model.addAttribute("Companies", companyService.getAllCompany());
				model.addAttribute("name", companyService.getCompanyById(id).getName());
				model.addAttribute("listPolicies", listPolicies);
				return "shop";
			}

		} catch (Exception e) {
			logger.info("content not found");
			return "shop";
		}
	}

	// view a proper policy
	@GetMapping("/shop/viewpolicy/{id}")
	public String viewPolicy(Model model, @PathVariable int id) {
		try {
			int year  = policyService.getPolicyById(id).get().getYear();
			if(year < 6) {
				model.addAttribute("msg","only "+year+" year left");
			}
			model.addAttribute("policy", policyService.getPolicyById(id).get());
			return "viewPolicy";
		} catch (Exception e) {
			logger.info("content not found");
			return "shop";
		}
	}

}
