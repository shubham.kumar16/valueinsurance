package com.vinove.ValueInsurance.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.service.CompanyService;
import com.vinove.ValueInsurance.service.PolicyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class AdminPolicyRestController {

	private static final Logger logger = LoggerFactory.getLogger(AdminPolicyRestController.class);

	public static String uploadDir = System.getProperty("user.dir") + "\\ValueInsurance\\src\\main\\resources\\static\\policyImages\\";

	@Autowired
	private PolicyService policyService;

	@Autowired
	private CompanyService companyService;

	// add policy in to database
	@RequestMapping(method = RequestMethod.POST, value = "/admin/addPolicy")
	public ResponseEntity<Policy> addPolicy(@Valid @RequestParam("id") int id, @RequestParam("name") String name,
											@RequestParam("company") int company, @RequestParam("price") int price, @RequestParam("year") int year,
											@RequestParam("claimable") double claimable, @RequestParam("description") String description,
											@RequestParam("policyImage") MultipartFile multipartFile,
											@RequestParam("imgName") String imgName) {
		try {
			Policy policy = new Policy();
			policy.setId(id);
			policy.setName(name);
			policy.setCompany(companyService.getCompanyById(company));
			policy.setPrice(price);
			policy.setClaimable(claimable);
			policy.setDescription(description);
			policy.setYear(year);
			String imageUUID;

			if (!multipartFile.isEmpty()) {
				imageUUID = multipartFile.getOriginalFilename();

				File file = new File(uploadDir+imageUUID);

				try (OutputStream os = new FileOutputStream(file)) {
					os.write(multipartFile.getBytes());
				}
				/*imageUUID = file.getOriginalFilename();
				Path fileNameAndPath = Paths.get(uploadDir, imageUUID);
				Files.write(fileNameAndPath, file.getBytes());*/
			} else {
				imageUUID = imgName;
			}
			policy.setImageName(imageUUID);
			policyService.addPolicy(policy);
			logger.info("policy added successfully");
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} catch (Exception e) {

			logger.warn(e.getMessage() +"please fill all the fields properly");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	// delete policy from database
	@RequestMapping(method = RequestMethod.DELETE, value = "/admin/deletePolicy")
	public ResponseEntity<Void> deletePolicy(@RequestParam("id") int id) {
		try {
			policyService.removePolicyById(id);
			logger.info("policy deleted successfully");
			return ResponseEntity.status(HttpStatus.ACCEPTED).build();
		} catch (Exception e) {
			logger.error("Data Integrity Violation Exception");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}

	// get policies from database
	@RequestMapping(method = RequestMethod.GET, value = "/admin/getPolicies")
	public ResponseEntity<List<Policy>> getAllPolicies(@RequestParam String keyword) {
		if(keyword == null) {			
			List<Policy> list = policyService.getAllPolicy();
			if (list.size() <= 0) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
			}
			return ResponseEntity.of(Optional.of(list));
		}else {
			List<Policy> list = policyService.getPolicyByKeyword(keyword);
			if (list.size() <= 0) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
			}
			return ResponseEntity.of(Optional.of(list));
		}
	}
}
