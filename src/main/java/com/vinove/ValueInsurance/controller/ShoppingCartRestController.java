package com.vinove.ValueInsurance.controller;

import java.time.LocalDate;
import java.util.List;

import com.vinove.ValueInsurance.model.CartItem;
import com.vinove.ValueInsurance.model.Order;
import com.vinove.ValueInsurance.model.Policy;
import com.vinove.ValueInsurance.model.User;
import com.vinove.ValueInsurance.service.OrderService;
import com.vinove.ValueInsurance.service.PolicyService;
import com.vinove.ValueInsurance.service.ShoppingCartServices;
import com.vinove.ValueInsurance.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.online.shopping.global.GlobalData;




@RestController
public class ShoppingCartRestController {

	@Autowired
	private ShoppingCartServices cartServices;

	@Autowired
	private UserService userService;

	@Autowired
	private PolicyService policyService;

	@Autowired
	private ShoppingCartServices shoppingCartServices;

	@Autowired
	private OrderService orderService;

	// add policy year in to cart 
	@PostMapping("/cart/add/{pid}/{qty}")
	public String addPolicyToCart(@PathVariable("pid") int policyId, @PathVariable("qty") int year,
			@AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		authentication = securityContext.getAuthentication();

		if (authentication == null) {
			return "You must login to add this policy to your shopping cart";
		} else {
			User user = userService.getCurrentLoggedInUser(authentication);
			User dbUser = userService.findUserByEmail(user.getEmail()).get();

			if (policyService.getById(policyId).getYear()-year >= 0) {
				int addedYear = cartServices.addPolicy(policyId, year, dbUser);
				return String.valueOf(addedYear);
			}
			return "NA";
		}
	}
	
	// update policy year in to cart 
	@PostMapping("/cart/update/{pid}/{qty}")
	public String updateYear(@PathVariable("pid") int policyId, @PathVariable("qty") int year,
			@AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		authentication = securityContext.getAuthentication();

		if (authentication == null) {
			return "You must login to update year ";
		} else {
			User user = userService.getCurrentLoggedInUser(authentication);
			User dbUser = userService.findUserByEmail(user.getEmail()).get();
			if (policyService.getById(policyId).getYear()-year >= 0) {
				float subtotal = cartServices.updateYear(policyId, year, dbUser);
				return String.valueOf(subtotal);
			}
			return "NA";
		}
	}

	// remove policy from cart 
	@PostMapping("/cart/remove/{pid}")
	public String removeFromCart(@PathVariable("pid") int policyId,
			@AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		authentication = securityContext.getAuthentication();

		if (authentication == null) {
			return "You must login to remove policy to your shopping cart";
		} else {
			User user = userService.getCurrentLoggedInUser(authentication);
			User dbUser = userService.findUserByEmail(user.getEmail()).get();
			cartServices.removePolicy(policyId, dbUser);
			return "removed";
		}
	}

	// remove all policy from cart
	@PostMapping("/cart/deleteAll")
	public String removeAll(@AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		authentication = securityContext.getAuthentication();

		if (authentication == null) {
			return "You must login to remove policy to your shopping cart";
		} else {
			User user = userService.getCurrentLoggedInUser(authentication);
			User dbUser = userService.findUserByEmail(user.getEmail()).get();
			cartServices.deleteAllPolicyByUser_Id(dbUser);
			return "removed";
		}
	}
	
	// total amount of policys
	@PostMapping("/cart/total/{total}")
	public String totalAmount(@PathVariable("total") String total,
			@AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		authentication = securityContext.getAuthentication();

		if (authentication == null) {
			return "You must login for total ";
		} else {
			User user = userService.getCurrentLoggedInUser(authentication);
			userService.findUserByEmail(user.getEmail()).get();
			GlobalData.amounts.add(0, Integer.parseInt(total));
			return "removed";
		}
	}

	// add order details in to database
	@PostMapping("/cart/order")
	public String orderDetails(@RequestParam String cOrderId, @AuthenticationPrincipal Authentication authentication) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		authentication = securityContext.getAuthentication();

		if (authentication == null) {
			return "You must login for order checkout";
		} else {
			User user = userService.getCurrentLoggedInUser(authentication);
			User dbUser = userService.findUserByEmail(user.getEmail()).get();
			List<CartItem> allPolicies = shoppingCartServices.findByUser_Id(dbUser.getId());
			GlobalData.amounts.clear();
			for (CartItem c : allPolicies) {
				Order order = new Order();
				order.setPolicy(c.getPolicy());
				order.setYear(c.getYear());
				Policy policy = policyService.findById(c.getPolicy().getId()).get();
				policy.setYear(policy.getYear() - c.getYear());
				policyService.save(policy);
				LocalDate myObj = LocalDate.now();
				order.setDate(myObj);
				order.setUser(dbUser);
				order.setCustomerOrderId(cOrderId);
				orderService.save(order);
			}
			return "order successful";
		}
	}

}
