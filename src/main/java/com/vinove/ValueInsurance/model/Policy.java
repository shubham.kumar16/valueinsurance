package com.vinove.ValueInsurance.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Entity
@Table
@Data
public class Policy implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5767364609563553794L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank()
	@Length(min = 2, max = 25)
	@Pattern(regexp = ".*([a-zA-Z]{2,25}$)")
	@Column(nullable = false, unique = true)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", referencedColumnName = "company_id")
	private Company company;

	@Min(value = 1)
	private int price;

	private double claimable;

	@NotBlank()
	@Column(nullable = false, unique = true)
	private String description;

	@NotBlank()
	@Column(nullable = false)
	private String imageName;

	@Column
	private int year;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getClaimable() {
		return claimable;
	}

	public void setClaimable(double claimable) {
		this.claimable = claimable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Policy(int id, String name, Company company, int price, double claimable, String description,
				  String imageName, int year) {
		super();
		this.id = id;
		this.name = name;
		this.company = company;
		this.price = price;
		this.claimable = claimable;
		this.description = description;
		this.imageName = imageName;
		this.year = year;
	}

	public Policy(String name, Company company, int price, double claimable, String description,
				  String imageName, int year) {
		super();
		this.name = name;
		this.company = company;
		this.price = price;
		this.claimable = claimable;
		this.description = description;
		this.imageName = imageName;
		this.year = year;
	}
	
	public Policy() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Policy [id=" + id + ", name=" + name + ", company=" + company + ", price=" + price + ", claimable="
				+ claimable + ", description=" + description + ", imageName=" + imageName + ", year=" + year + "]";
	}

	
}
