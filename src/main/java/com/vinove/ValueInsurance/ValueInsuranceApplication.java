package com.vinove.ValueInsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValueInsuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValueInsuranceApplication.class, args);
	}

}


