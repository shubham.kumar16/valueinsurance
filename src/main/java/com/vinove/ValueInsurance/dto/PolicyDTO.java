package com.vinove.ValueInsurance.dto;

import org.springframework.web.multipart.MultipartFile;

public class PolicyDTO {

	private int id;
	private String name;
	private int companyId;
	private int price;
	private double claimable;
	private String description;
	private String imageName;
	private int year;
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	private MultipartFile policyImage;


	public MultipartFile getPolicyImage() {
		return policyImage;
	}

	public void setPolicyImage(MultipartFile policyImage) {
		this.policyImage = policyImage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getClaimable() {
		return claimable;
	}

	public void setClaimable(double claimable) {
		this.claimable = claimable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public PolicyDTO(int id, String name, int companyId, int price, double claimable, String description,
					 String imageName, int year, MultipartFile policyImage) {
		super();
		this.id = id;
		this.name = name;
		this.companyId = companyId;
		this.price = price;
		this.claimable = claimable;
		this.description = description;
		this.imageName = imageName;
		this.year = year;
		this.policyImage = policyImage;
	}
	
	public PolicyDTO(String name, int companyId, int price, double claimable, String description,
					 String imageName, int year, MultipartFile policyImage) {
		super();
		this.name = name;
		this.companyId = companyId;
		this.price = price;
		this.claimable = claimable;
		this.description = description;
		this.imageName = imageName;
		this.year = year;
		this.policyImage = policyImage;
	}


	public PolicyDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "PolicyDTO [id=" + id + ", name=" + name + ", companyId=" + companyId + ", price=" + price
				+ ", claimable=" + claimable + ", description=" + description + ", imageName=" + imageName + ", year="
				+ year + ", policyImage=" + policyImage + "]";
	}

}
